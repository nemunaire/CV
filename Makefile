all: cv.pdf

.tex.pdf:
	latexmk --xelatex $<

clean:
	latexmk -c

.SUFFIXES: .sh .tex .pdf
